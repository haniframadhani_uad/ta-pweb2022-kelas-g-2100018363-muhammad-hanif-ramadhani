<?php
// program 10.2.1
echo "program 10.2.1<br>";
$nilai = 50;
if ($nilai >= 60) {
    echo "nilai anda $nilai, anda LULUS";
} else {
    echo "nilai anda $nilai, anda GAGAL";
}
echo "<br>";
// program 10.2.2
echo "<br>program 10.2.2<br>";
$i = 0;
$j = 0;
$tinggi = 5;
for ($baris = 1; $baris <= $tinggi; $baris++) {
    for ($i = 1; $i <= $tinggi - $baris; $i++) {
        echo "&ensp;";
    }
    for ($j = 1; $j < $baris * 2; $j++) {
        echo "*";
    }
    echo "<br>";
}