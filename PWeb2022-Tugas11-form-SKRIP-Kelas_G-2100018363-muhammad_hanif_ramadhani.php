<?php
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if (empty($_POST["name"])) {
		$nameErr = "name is required";
	} else {
		if (!preg_match("/^[a-zA-Z ]*$/", $_POST["name"])) {
			$nameErr = "only letter and white space allowed";
		} else {
			$name = test_input($_POST["name"]);
		}
	}
	if (empty($_POST["email"])) {
		$emailErr = "email is required";
	} else {
		if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$emailErr = "email format is invalid";
		} else {
			$email = test_input($_POST["email"]);
		}
	}
	if (empty($_POST["website"])) {
		$website = "";
	} else {
		if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www.\))[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $website)) {
			$websiteErr = "invalid URL";
		} else {
			$website = test_input($_POST["website"]);
		}
	}
	if (empty($_POST["comment"])) {
		$comment = "";
	} else {
		$comment = test_input($_POST["comment"]);
	}
	if (empty($_POST["gender"])) {
		$genderErr = "gender is required";
	} else {
		if ($_POST["gender"] != "male" && $_POST["gender"] != "female") {
			$genderErr = "<strong>FUCK YOU!</strong> gender is only two";
		} else {
			$gender = test_input($_POST["gender"]);
		}
	}
}

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form</title>
    <style>
    .error {
        color: red;
    }
    </style>
</head>

<body>
    <h2>php form validation example</h2>
    <p><span class="error">* required field</span></p>
    <form method="post" action="">
        <label for="name">name : </label>
        <input id="name" type="text" name="name" value="<?= $name; ?>" required>
        <span class="error">* <?= $nameErr ?></span>
        <br><br>
        <label for="email">e-mail : </label>
        <input id="email" type="email" name="email" value="<?= $email; ?>" required>
        <span class="error">* <?= $emailErr ?></span>
        <br><br>
        <label for="website">website : </label>
        <input id="website" type="text" name="website" value="<?= $website ?>">
        <span class="error">* <?= $websiteErr ?></span>
        <br><br>
        <label for="comment">comment : </label>
        <textarea id="comment" name="comment" cols="40" rows="5"><?= $comment ?></textarea>
        <br><br>
        <label for="gender">gender : </label>
        <input id="female" type="radio" name="gender"
            <?php if (isset($gender) && $gender == "female") echo "checked;" ?> value="female">
        <label for="female">female</label>
        <input id="male" type="radio" name="gender" <?php if (isset($gender) && $gender == "male") echo "checked"; ?>
            value="male">
        <label for="male">male</label>
        <input id="male" type="radio" name="gender"
            <?php if (isset($gender) && $gender == "non-biner") echo "checked"; ?> value="non-biner">
        <label for="non-biner">non-biner</label>
        <span class="error">* <?= $genderErr ?></span>
        <br><br>
        <input id="submit" type="submit" name="submit" value="submit">
    </form>
    <h2>your inpur :</h2>
    <?php if (isset($_POST["submit"]) && $name != "" && $email != "" && $gender != "") : ?>
    <p>name : <?= $name ?></p>
    <p>email : <?= $email ?></p>
    <p>website : <?php if ($website != "") {
										echo $website;
									} else {
										echo "-";
									} ?></p>
    <p>commet : <?php if ($comment != "") {
									echo $comment;
								} else {
									echo "-";
								} ?></p>
    <p>gender : <?= $gender ?></p>
    <?php endif; ?>
</body>

</html>