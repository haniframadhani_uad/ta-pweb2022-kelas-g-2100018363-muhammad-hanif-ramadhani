<?php
// program 9.1
$gaji = 1000000;
$pajak = 0.1;
$thp = $gaji - ($gaji * $pajak);

echo "program 9.1<br>";
echo "Gaji sebelum pajak = Rp. $gaji <br>";
echo "Gaji yang dibawa pulang = Rp. $thp";

// program 9.2
$a = 5;
$b = 4;

echo "<br><br>program 9.2<br>";
echo "$a == $b : " . ($a == $b);
echo "<br>$a != $b : " . ($a != $b);
echo "<br>$a > $b : " . ($a > $b);
echo "<br>$a < $b : " . ($a < $b);
echo "<br>($a == $b) && ($a > $b) : " . ($a == $b) && ($a > $b);
echo "<br>($a == $b) || ($a > $b) : " . ($a == $b) || ($a > $b);