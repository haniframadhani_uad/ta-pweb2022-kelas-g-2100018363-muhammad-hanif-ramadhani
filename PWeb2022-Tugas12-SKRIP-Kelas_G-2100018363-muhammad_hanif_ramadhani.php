<?php
// $arrBuah = array("mangga", "apel", "pisang", "jeruk");
// echo $arrBuah[0] . "<br>";
// echo $arrBuah[3] . "<br><br>";

// $arrWarna = array();
// $arrWarna[] = "merah";
// $arrWarna[] = "biru";
// $arrWarna[] = "hijau";
// $arrWarna[] = "putih";
// echo $arrWarna[0] . "<br>";
// echo $arrWarna[2] . "<br>";

// $arrNilai = array("fulan" => 80, "fulin" => 90, "fulun" => 75, "falan" => 85);
// echo $arrNilai['fulan'] . "<br>";
// echo $arrNilai['fulin'] . "<br><br>";

// $arrNilai = array();
// $arrNilai['amin'] = 80;
// $arrNilai['aman'] = 95;
// $arrNilai['amen'] = 77;
// echo $arrNilai['aman'] . "<br>";
// echo $arrNilai['amin'] . "<br>";

// $arrWarna = array("red", "orange", "yellow", "green", "blue", "purple");

// echo "menampilkan isi array dengan for : <br>";
// for ($i = 0; $i <br count($arrWarna); $i++) {
//     echo "warna penlangi <font color=$arrWarna[$i]>" . $arrWarna[$i] . "</font><br>";
// }

// echo "<br>menampilkan isi array dengan foreach : <br>";
// foreach ($arrWarna as $warna) {
//     echo "warna penlangi <font color=$warna>" . $warna . "</font><br>";
// }

// $arrNilai = array("fulan" => 80, "fulin" => 90, "fulun" => 75, "falan" => 85);
// echo "menampilkan isi array asosiatif dengan foreach: <br>";
// foreach ($arrNilai as $nama => $nilai) {
//     echo "nilai $nama = $nilai<br>";
// }

// reset($arrNilai);
// echo "<br>menampilkan isi array asosiatif dengan while dan list : <br>";
// while (list($nama, $nilai) = each($arrNilai)) {
//     echo "nilai $nama = $nilai<br>";
// }

// $arrNilai = array("fulan" => 80, "fulin" => 90, "fulun" => 75, "falan" => 85);
// echo "</b>array sebelum diurutkan</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// sort($arrNilai);
// reset($arrNilai);
// echo "</b>array setelah diurutkan dengan sort()</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// rsort($arrNilai);
// reset($arrNilai);
// echo "</b>array setelah diurutkan dengan rsort()</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// asort($arrNilai);
// reset($arrNilai);
// echo "</b>array setelah diurutkan dengan asort()</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// arsort($arrNilai);
// reset($arrNilai);
// echo "</b>array setelah diurutkan dengan arsort()</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// ksort($arrNilai);
// reset($arrNilai);
// echo "</b>array setelah diurutkan dengan ksort()</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// krsort($arrNilai);
// reset($arrNilai);
// echo "</b>array setelah diurutkan dengan krsort()</b>";
// echo "<pre>";
// print_r($arrNilai);
// echo "</pre>";

// $transport = array('jalan kaki', 'onthel', 'mobil', 'pesawat');
// echo "<pre>";
// print_r($transport);
// echo "</pre>";
// $mode = current($transport);
// echo $mode . "<br>";
// $mode = next($transport);
// echo $mode . "<br>";
// $mode = current($transport);
// echo $mode . "<br>";
// $mode = prev($transport);
// echo $mode . "<br>";
// $mode = end($transport);
// echo $mode . "<br>";
// $mode = current($transport);
// echo $mode . "<br>";

// $arrBuah = array('mangga', 'apel', 'apel', 'kedondong', 'jeruk');
// if (in_array("kedondong", $arrBuah)) {
//     echo "ada buah kedondong di dalam array tsb!";
// } else {
//     echo "tidak ada buah kedondong di array tersebut";
// }

// function cetak_ganjil($awal, $akhir)
// {
// 	for ($i = $awal; $i < $akhir; $i++) {
// 		if ($i % 2 == 1) {
// 			echo "$i, ";
// 		}
// 	}
// }
// $a = 10;
// $b = 50;
// echo "bilangan ganjil dari $a sampai $b, adalah : <br>";
// cetak_ganjil($a, $b);

// function luas_lingkaran($jari)
// {
// 	return 3.14 * $jari * $jari;
// }
// $r = 10;
// echo "luas lingkaran dengan jari-jari $r = ";
// echo luas_lingkaran($r);

// function tambah_string(&$str)
// {
// 	$str = $str . ", yogyakarta";
// 	return $str;
// }
// $string = "universitas ahmad dahlan";
// echo "\$string = $string<br>";
// echo tambah_string($string) . "<br>";
// echo "\$string = $string<br>";

$arr = get_defined_functions();
echo "<pre>";
print_r($arr);
echo "</pre>";